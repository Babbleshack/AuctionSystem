package auctionClients;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.security.*;
import javax.crypto.*;

import storageObjects.AuctionItem;
import storageObjects.Bid;
import storageObjects.Product;
import storageObjects.Profile;
import storageObjects.Challenge;
import keyManager.KeyManager;
import auctionServer.IAuctionProccessor;

public class BuyingClient {
  private static final String DEFAULT_URI = "rmi://localhost/AuctionService";
  private static final String SERVER_KEY = "./keys/SERVER_PUBLIC_KEY";
  private static final String ALICE_KEY = "./keys/ALICE_PRIVATE_KEY";
  private static final String BOB_KEY = "./keys/BOB_PRIVATE_KEY";
  private static IAuctionProccessor auctionServer;
  private static KeyManager km;
  private static PublicKey serverPK;
  private static HashMap<String, PrivateKey> rsaPrivateKeys;
  private static Key ticket;
  private static SealedObject serverReply;
  public BuyingClient(/*SERVER URI*/)
  {
    auctionServer = attemptConnection();
    if(auctionServer == null)
    {
      System.err.println("NO CONNECTION...");
      System.exit(1);
    }
      km = new KeyManager();
      rsaPrivateKeys = new HashMap<String, PrivateKey>();
      rsaPrivateKeys.put("alice", km.readPrivateKeyFromFile(ALICE_KEY));
      rsaPrivateKeys.put("bob", km.readPrivateKeyFromFile(BOB_KEY));
      serverPK = km.readPublicKeyFromFile(SERVER_KEY);
      System.out.println("Keys Read...");            
  }
  private static IAuctionProccessor attemptConnection(/*SERVER URI*/)
  {
    try {
            System.out.println("Attempting Server Connection");			
            IAuctionProccessor server = (IAuctionProccessor)
                                    Naming.lookup(DEFAULT_URI);
            System.out.println(server.toString());
            System.out.println("Connected to server!");
            return server;
    } catch (MalformedURLException e) {
            System.err.println("Malformed URL");
            e.printStackTrace();
    } catch (RemoteException e) {
            System.err.println("Remote Exception Server Could Not be Found");
            e.printStackTrace();
    } catch (NotBoundException e) {
            System.err.println("Class has no known bound association in registry");
            e.printStackTrace();
    }
    return null;
  }  
  public static void main(String[] args)
  {
    BuyingClient bc = new BuyingClient(); 
    Scanner s = new Scanner(System.in);
    ArrayList<Product> auctions;
    int response;
    Challenge challenge;
    int challengeValue;
    String aid;
    String name;
    String username;
    String email;
    double bidValue;
    SealedObject serverResponse;
    boolean mayProceed;
    Bid bid;
    Profile profile;
    System.out.println("WELCOME TO AUCTION BUYING CLIENT");
    System.out.println("===============================");
    //Start Login loop
    while(true)
    { 
          //Security Manager
      if (System.getSecurityManager() == null) {
        System.setSecurityManager(new RMISecurityManager());
      }
      response = 0;
      name = "";
      System.out.println("Please enter your username...");
      name = s.nextLine();
      if(name == null)
      {
        System.out.println("Please enter a valid string");
        continue;
      } else if (!rsaPrivateKeys.containsKey(name.toLowerCase())) {
        System.out.println("User does not exist");
        continue;
      } 
      name = name.toLowerCase(); //get lower case representation
      try {
        challengeValue = km.generateChallenge();
        challenge = new Challenge(name);
        challenge.setChallenge(challengeValue);
        serverReply = auctionServer.buyerSignIn(name, km.encryptRSAObject(serverPK, challenge));
        if(serverReply == null)
        {
          System.out.println("Bad Server");
          continue;
        }
        PrivateKey userPrivate = rsaPrivateKeys.get(name);
        challenge = (Challenge)km.decryptRSAObject(userPrivate, serverReply);
        if(challenge.getChallenge() != challengeValue)
        {
          System.out.println("Connection insecure...");
          System.exit(0);
        }

        ticket = km.getKeyFromBytes(challenge.getKey());
      } catch (Exception e) {
        System.err.println("Error doing Handshake");
        e.printStackTrace();
      } finally { //clean up
        challengeValue = 0;
        serverReply = null;
        challenge = null;
        username = name;
        name = null;
      }
      break;
    }
    //Do Work Loop
    while(true)
    {
      response = 0;
      aid = name = email = null;
      bidValue = 0.0;

      System.out.println("===============================");
      System.out.println("1)\tList all Auctions");
      System.out.println("2)\tMake Bid");
      System.out.println("-1)\tQuit");
      System.out.println("===============================");
      response = s.nextInt();
      s.nextLine();
      if(response == 1)
      {
        try{
          serverReply = auctionServer.getAuctions(username); 
          if(serverReply == null)
            continue;
          
        } catch (Exception e) {
          System.err.println("Error getting Auctions");
          e.printStackTrace();
        }
          auctions = (ArrayList<Product>)km.decryptAESObject(ticket, serverReply);
          if(auctions == null)
          {
                  System.out.println("NO AUCTIONS FOUND or Decyption failed");
                  continue;
          }
          for(Product p: auctions)
          {
                  System.out.println("=====================");
                  System.out.println("Auction ID: " + p.getAuctionUID() + "\n");
                  System.out.println("Product Title: " + p.getTitle() + "\n");
                  System.out.println("Current Bid Value: £" + 
                  p.getCurrentBidValue());
          }
          auctions = null;
          serverReply = null;
      } else if (response == 2) {
          System.out.println("Please enter the Auction ID of the product");
          aid = s.nextLine();
          System.out.println("Please enter the ammount you wish to bid");
          bidValue = s.nextDouble();
          s.nextLine();
          System.out.println("Please enter your name");
          name = s.nextLine();
          System.out.println("Please enter your email");
          email = s.nextLine();
          profile = new Profile(name, email);
          bid = new Bid(aid, profile, bidValue);
          try {
            auctionServer.makeBid(username, km.encryptAESObject(ticket, bid));
          } catch (Exception e) {
            System.err.println("Error making Bid");
            e.printStackTrace();
          } finally {
            profile = null;
            bid = null;
          }
      } else if (response == -1) {
          break;
      } else {
          continue;
      }
    }
    System.out.println("GOODBYE!");
    s.close();
    System.exit(0);
  }
}
