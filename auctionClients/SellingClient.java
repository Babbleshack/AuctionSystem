package auctionClients;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.security.*;
import javax.crypto.*;

import storageObjects.Bid;
import storageObjects.Product;
import storageObjects.Profile;
import storageObjects.AuctionItem;
import auctionServer.IAuctionProccessor;
import storageObjects.Challenge;
import keyManager.KeyManager;

public class SellingClient {
	private static IAuctionProccessor auctionServer;
	private static final int CREATE_AUCTION = 1;
	private static final int CLOSE_AUCTION = 2;
	private static final int END_PROGRAM = -1;
	private static final String DEFAULT_URI = "rmi://localhost/AuctionService";
  	private static final String SERVER_KEY = "./keys/SERVER_PUBLIC_KEY";
    private static final String ALICE_KEY = "./keys/ALICE_PRIVATE_KEY";
    private static final String BOB_KEY = "./keys/BOB_PRIVATE_KEY";
    private static KeyManager km;
    private static PublicKey serverPK;
    private static HashMap<String, PrivateKey> rsaPrivateKeys;
    private static Key ticket;
    private static SealedObject serverReply;

	public SellingClient()
	{

 		//Security Manager
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}	

  	 auctionServer = attemptConnection();
    if(auctionServer == null)
    {
      System.err.println("NO CONNECTION...");
      System.exit(1);
    }
      km = new KeyManager();
      rsaPrivateKeys = new HashMap<String, PrivateKey>();
      rsaPrivateKeys.put("alice", km.readPrivateKeyFromFile(ALICE_KEY));
      rsaPrivateKeys.put("bob", km.readPrivateKeyFromFile(BOB_KEY));
      serverPK = km.readPublicKeyFromFile(SERVER_KEY);
      System.out.println("Keys Read...");  		auctionServer = attemptConnection();
	}
	private IAuctionProccessor attemptConnection(/*SERVER URI*/)
	{
		try {
			System.out.println("Attempting Server Connection");			
			IAuctionProccessor server = (IAuctionProccessor)
						Naming.lookup(DEFAULT_URI);
			System.out.println(server.toString());
			System.out.println("Connected to server!");
			return server;
		} catch (MalformedURLException e) {
                        System.err.println("Malformed Exception");
			e.printStackTrace();
		} catch (RemoteException e) {
			System.err.println("Remote Server Error");
			e.printStackTrace();
		} catch (NotBoundException e) {
			System.err.println("Remote class not bound");
			e.printStackTrace();
		}
		return null;
	}
	public IAuctionProccessor getServer()
	{
		return this.auctionServer;
	}

	public static void main(String[] args) {
		int response;
		double reserve;
		String title;
		String uid;
		String name;
		String username;
		int challengeValue;
		Challenge challenge;
		int count = 0;
		SellingClient sc = new SellingClient(); 
		AuctionItem auction;
		Scanner s = new Scanner(System.in);
		System.out.println("WELCOME TO AUCTION SELLING CLIENT");
		System.out.println("===============================");
	//Start Login loop
    while(true)
    { 
      response = 0;
      name = "";
      System.out.println("Please enter your username...");
      name = s.nextLine();
      if(name == null)
      {
        System.out.println("Please enter a valid string");
        continue;
      } else if (!rsaPrivateKeys.containsKey(name.toLowerCase())) {
        System.out.println("User does not exist");
        continue;
      } 
      name = name.toLowerCase(); //get lower case representation
      try {
        challengeValue = km.generateChallenge();
        challenge = new Challenge(name);
        challenge.setChallenge(challengeValue);
        serverReply = auctionServer.sellerSignIn(name, km.encryptRSAObject(serverPK, challenge));
        if(serverReply == null)
        {
        	System.out.println("Bad Server");
        	continue;
        }
        PrivateKey userPrivate = rsaPrivateKeys.get(name);
        challenge = (Challenge)km.decryptRSAObject(userPrivate, serverReply);
        if(challenge.getChallenge() != challengeValue)
        {
          System.out.println("Connection insecure...");
          System.exit(0);
        }
        ticket = km.getKeyFromBytes(challenge.getKey());
      } catch (Exception e) {
        System.err.println("Error doing Handshake");
        e.printStackTrace();
      } finally { //clean up
        challengeValue = 0;
        serverReply = null;
        challenge = null;
        username = name;
        name = null;
      }
      break;
    }
    //main loop
	while(true)
	{
		auction = null;
		response = 0;
		reserve = 0;
		uid = title = null;

		System.out.println("===============================");
		System.out.println("1)\tCreate Auction");
		System.out.println("2)\tClose Auction");
		System.out.println("-1)\tQuit");
		System.out.println("===============================");
		try{
              response = s.nextInt();
            } catch(InputMismatchException e) {
              System.out.println("Please input a valid value");
              continue;
            }
		s.nextLine();
		if(response == CREATE_AUCTION)
		{
			System.out.println("Please enter the Name of the product");
			title = s.nextLine();
			System.out.println("Please enter the reserve price");
			reserve = s.nextDouble();
			System.out.println("reserve: " + reserve);
			s.nextLine();
			try {
				serverReply = sc.getServer().createAuction(username, km.encryptAESObject(ticket, new AuctionItem(title, reserve, username))); 
				if(serverReply == null)
				{
					System.out.println("error creating auction");
					continue;
				}
				if(serverReply == null)
					System.out.println("Auction was not created");
				auction = (AuctionItem)km.decryptAESObject(ticket, serverReply);
				System.out.println("AuctionCreated with ID [" + auction.getUID() + "]");
			} catch (RemoteException e) {
				System.err.println("Error proccessing Auction");
				e.printStackTrace();
			}
		} else if (response == CLOSE_AUCTION) {
				System.out.println("Please enter the UID of the auction You want to close");
				uid = s.nextLine();
				if(uid.equals(""))
				{
					System.err.println("invalid UID");
					continue;
				}
				try {
					auction = new AuctionItem(uid);
					auction.setOwner(username);
					serverReply =  sc.getServer().closeAuction(username, km.encryptAESObject(ticket, auction));
					if(serverReply == null)
					{
						System.out.println("error closing auction");
						continue;
					}
					auction = (AuctionItem)km.decryptAESObject(ticket, serverReply);
				} catch (RemoteException e) {
					System.err.println("Error closing auction");
					e.printStackTrace();
				}	
				if(auction == null)
					System.out.println("ERROR: Auction could not be found!");
				System.out.println("Auction [" + auction.getUID() + "]");
				if(auction.getBidValue() <= 0)
				{
					System.out.println("Unfortunatly your reserve was never met and was not sold!");
					continue;
				}
				System.out.println("Winnder Details");
				System.out.println("The Final Bid price was: [£" + auction.getBidValue() + "]");
				System.out.println("Name: " + auction.getBid().getBidderName());
				System.out.println("Email: " + auction.getBid().getBidderEmail());
		} else if(response == END_PROGRAM){
			break;
		}
	}
	System.out.println("GOODBYE!");
	s.close();

}

}
