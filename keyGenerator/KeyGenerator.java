package keyGenerator;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.io.*;
import java.math.BigInteger;

public class KeyGenerator 
{
  /**
   * Generates RSA Keys for server an user in a static context
   * would not be used in real world implementations
   * @author Dominic Lindsay
   * */
  public static void main(String[] args)
  {
    genearateAndWriteKeyPair("ALICE");
    genearateAndWriteKeyPair("BOB");
    genearateAndWriteKeyPair("SERVER");
  }
  private static void genearateAndWriteKeyPair(String user)
  {
    try {
      KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
      /**
       * Generate pub/private key pair
       * using DSA 2048 increption
       **/
      keyGen.initialize(2048);
      KeyPair kp = keyGen.generateKeyPair();
      PublicKey publicKey = kp.getPublic();  
      PrivateKey privateKey = kp.getPrivate();
      //get components of key
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");  
      RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);  
      RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);  
      writeKeyToFile(user + "_PUBLIC_KEY", rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
      writeKeyToFile(user + "_PRIVATE_KEY", rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent());
    } catch (Exception e) {
        System.err.println("Error generating and writing key pair \n");
        e.printStackTrace();
    }    
  }
  private static void writeKeyToFile(String filename, BigInteger mod, BigInteger exp) 
    throws Exception
  {
    FileOutputStream fos = null;
    ObjectOutputStream oos = null;
    try{
      fos = new FileOutputStream("./keys/" + filename);
      oos = new ObjectOutputStream(new BufferedOutputStream(fos));
      oos.writeObject(mod);
      oos.writeObject(exp);
    } catch (Exception e) {
      System.err.println("Error writing key file");
      e.printStackTrace();
    } finally { //make sure streams are closed
      if(oos != null)
        oos.close();
        if(fos != null)
          fos.close();
      System.out.println(filename + " KEY FILE WRITTEN");
      return;
    }
  }
}
