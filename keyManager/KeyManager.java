package keyManager;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.io.*;
import java.io.Serializable;
import java.util.*;
import java.math.BigInteger;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
public class KeyManager {
  public KeyManager(){
    System.out.println("Key Manager Started"); //DEBUG
  }
  public PublicKey readPublicKeyFromFile(String keyFileName) {
    FileInputStream fis = null;
    ObjectInputStream ois = null;
    try {
      fis = new FileInputStream(new File(keyFileName));
      ois = new ObjectInputStream(fis);
     
      BigInteger modulus = (BigInteger) ois.readObject();
      BigInteger exponent = (BigInteger) ois.readObject();

      //Get Public Key
      RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, exponent);
      KeyFactory fact = KeyFactory.getInstance("RSA");
      PublicKey publicKey = fact.generatePublic(rsaPublicKeySpec);
      ois.close();
      fis.close();         
      return publicKey;  
    } catch (Exception e) {
     e.printStackTrace();
    }
    return null;
  }
  public PrivateKey readPrivateKeyFromFile(String keyFileName) {
    FileInputStream fis = null;
    ObjectInputStream ois = null;
    try {
      fis = new FileInputStream(new File(keyFileName));
      ois = new ObjectInputStream(fis);
     
      BigInteger modulus = (BigInteger) ois.readObject();
      BigInteger exponent = (BigInteger) ois.readObject();

      //Get Private Key
      RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modulus, exponent);
      KeyFactory fact = KeyFactory.getInstance("RSA");
      PrivateKey privateKey = fact.generatePrivate(rsaPrivateKeySpec);
      ois.close();
      fis.close();         
      return privateKey;  
    } catch (Exception e) {
     e.printStackTrace();
    }
    return null;
  }
 /**
  * Encrypt Data returning sealedObject
  * @param data
  */
  public SealedObject encryptRSAObject(PublicKey publickey, Serializable object) {
    PublicKey pk = publickey;
    SealedObject so = null;
    try {
      Cipher cipher = Cipher.getInstance("RSA");
      cipher.init(Cipher.ENCRYPT_MODE, pk);
      so = new SealedObject(object, cipher);
      return so;
    } catch (Exception e) {
      System.err.println("ERROR SEALING OBJECT");
      e.printStackTrace();
    }  
    return so;
  }
 /**
  * Decrypt Data returning decrypted Object
  * @param data
  */
  public Object decryptRSAObject(PrivateKey privatekey, SealedObject so) {
    PrivateKey pk = privatekey;
    try {
      Cipher cipher = Cipher.getInstance("RSA");
      cipher.init(Cipher.DECRYPT_MODE, pk);
      return so.getObject(pk);
    } catch (Exception e) {
      System.err.println("ERROR SEALING OBJECT");
      e.printStackTrace();
    }  
    return null;
  }
  /**
   * Generate AES Key
   */
  public Key generateAESKey()
  {
    try{
      KeyGenerator keyGen = KeyGenerator.getInstance("AES");
      keyGen.init(128); 
      return keyGen.generateKey();    
    } catch (Exception e) {
      System.err.println("Error generating AESKey");
      e.printStackTrace();
    }
    return null;
  }

  public SealedObject encryptAESObject(Key privateKey, Serializable o)
  {
      Key pk = privateKey;
      try {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, pk);
        return new SealedObject(o, cipher);
      } catch (Exception e) {
        System.err.println("Error Encrypting Object(AES)");
        e.printStackTrace();
      }
      return null;
  }
  public Object decryptAESObject(Key privatekey, SealedObject so) {
    Key pk = privatekey;
    try {
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.DECRYPT_MODE, pk);
      return so.getObject(pk);
    } catch (Exception e) {
      System.err.println("ERROR SEALING OBJECT (AES)");
      e.printStackTrace();
    }  
    return null;
  }
  public int generateChallenge()
  {
    Random r = new Random();
    return r.nextInt((100 - 0) + 1);
  }
  public Key getKeyFromBytes(byte[] key)
  {
    try{
      SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
      return(Key) keySpec;
    } catch (Exception e) {
      System.out.println("Error getting key from bytes");
      e.printStackTrace();
    }
    return null;
  }
  
}