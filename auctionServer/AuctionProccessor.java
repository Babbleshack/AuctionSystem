/**
 * IAuctionServ implementation
 */
package auctionServer;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.security.*;
import javax.crypto.*;

import storageObjects.AuctionItem;
import storageObjects.Bid;
import storageObjects.Product;
import storageObjects.Challenge;
import keyManager.KeyManager;
/**
 * Auction:
 * 		java.rmi.server.UID uid
 *		Product product 
 *		Bid highestBid 
 *		double reserve
 *		
 * Bid:
 * 	String clientName
 * 	String clientEmail
 *	double bidValue	
 * 
 * Product:
 * 		String Name
 *		String Description
 */
@SuppressWarnings("serial")
public class AuctionProccessor 
	extends UnicastRemoteObject implements IAuctionProccessor, Serializable
{
	private static final String BOB_PUB = "./keys/BOB_PUBLIC_KEY";
	private static final String ALICE_PUB = "./keys/ALICE_PUBLIC_KEY";
	private static final String SERVER_PRIVATE = "./keys/SERVER_PRIVATE_KEY";
	
	private HashMap<String, AuctionItem> auctions; //map of Products 
	private HashMap<String, PublicKey> rsaKeys; //map of user public keys
	private HashMap<String, Key> activeBuyerKeys; //map of active Buyer keys 
	private HashMap<String, Key> activeSellerKeys; //map of active Seller keys 
	private PrivateKey serverPrivateKey; //Server private key 
	private KeyManager km = new KeyManager();
	public AuctionProccessor()
		throws RemoteException
	{
		super();

				//Security Manager
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		this.auctions = new HashMap<String, AuctionItem>();
		this.rsaKeys = new HashMap<String, PublicKey>();
		this.activeBuyerKeys = new HashMap<String, Key>();
		this.activeSellerKeys = new HashMap<String, Key>();
		//load user public keys
		rsaKeys.put("bob", km.readPublicKeyFromFile(BOB_PUB));
		rsaKeys.put("alice", km.readPublicKeyFromFile(ALICE_PUB));
		//load server private key
		serverPrivateKey = km.readPrivateKeyFromFile(SERVER_PRIVATE);		
	} 	
	public SealedObject createAuction(String username, SealedObject sealedAuction)
		throws RemoteException
	{
		if(!activeSellerKeys.containsKey(username))
			return null; //not a seller
		AuctionItem auctionItem = (AuctionItem)km.decryptAESObject(activeSellerKeys.get(username), sealedAuction);
		if(auctionItem == null)
			return null;
		auctions.put(auctionItem.getUID(), auctionItem);
		System.out.println("Auction created with UID: [" + auctionItem.getUID() + "]");
		System.out.println("Reserver Price " + auctionItem.getReservePrice());
		Key key = activeSellerKeys.get(username);
		return km.encryptAESObject(key, auctionItem);
	}
	public SealedObject closeAuction(String username, SealedObject sealedAuction) 
		throws RemoteException
	{
		if(!activeSellerKeys.containsKey(username))
			return null; //not a seller
		Key key = activeSellerKeys.get(username);
		AuctionItem ai = (AuctionItem)km.decryptAESObject(key, sealedAuction);
		String proposedAuctionID = ai.getUID();
		if(!auctions.containsKey(proposedAuctionID))
			return null; //create exception for this task
		AuctionItem auction = auctions.get(proposedAuctionID);
		if(!ai.getOwner().equals(auction.getOwner()))
			return null;
		return km.encryptAESObject(activeSellerKeys.get(username), auctions.remove(ai.getUID())); //remove and return
	}
	public void makeBid(String username, SealedObject sealedBid) 
		throws RemoteException
	{
		if(!activeBuyerKeys.containsKey(username))
			return; //do nothing (protect usernames)
		Bid bid = (Bid)km.decryptAESObject(activeBuyerKeys.get(username), sealedBid);
		if(bid == null)
			return; //dodgy encryption
		if(!auctions.containsKey(bid.getProductUID()))
			return;//need exception2
		AuctionItem auction = auctions.get(bid.getProductUID()); 
		//if(!auction.setBid(bid))
			//return; // need bid not set exception
		System.out.println("Current bid price: \t" + auction.getBidValue() + "\n" +
			"Counter Bid value: \t" + bid.getBidValue());
		//if(auction.getReservePrice() > bid.get)
		if(!auction.isBidHigher(bid))
			return; // Bid not set as original bid is greater
		auction.setBid(bid); //set the bid
		System.out.println("Bid Made with value: \t[" + bid.getBidValue() + "]\n" + 
			"User: \t[" + bid.getBidder().getName() + "]\n" + 
			"Email: \t[" + bid.getBidder().getEmail() + "]\n");
	}
	public SealedObject getAuctions(String username)
		throws RemoteException
	{
		if(!activeBuyerKeys.containsKey(username))
				return null; //do nothing (protect usernames)
		List<AuctionItem> auctionItems = new ArrayList<AuctionItem>(this.auctions.values());
		ArrayList<Product> products = new ArrayList<Product>();
		for(AuctionItem ai: auctionItems)
			products.add(new Product(ai.getProductTitle(),
					ai.getUID(), ai.getBidValue() == Bid.FALSE_VALUE ? 0 : ai.getBidValue()));
		System.out.println("SENDING PRODUCTS: [" + products.size() + "]");
		return km.encryptAESObject(activeBuyerKeys.get(username), products);
	}
	/**
	 * Sign seller account in
	 * @param  username        username to be used
	 * @param  sealedChallenge [description]
	 * @return                 [description]
	 */
	public SealedObject sellerSignIn(String username, SealedObject sealedChallenge)
		throws RemoteException
	{
      if(!rsaKeys.containsKey(username))
      	return null;
      PublicKey pk = rsaKeys.get(username);
      Challenge challenge = (Challenge) km.decryptRSAObject(serverPrivateKey, sealedChallenge);
      Key userKey = km.generateAESKey();
      activeSellerKeys.put(username, userKey);
      challenge.setKey(userKey.getEncoded());
      return km.encryptRSAObject(pk, challenge);
	}
	public SealedObject buyerSignIn(String username, SealedObject sealedChallenge)
		throws RemoteException
	{
      if(!rsaKeys.containsKey(username))
      	return null;
      PublicKey pk = rsaKeys.get(username);
      Challenge challenge = (Challenge)km.decryptRSAObject(serverPrivateKey, sealedChallenge);
      challenge.setKey(km.generateAESKey().getEncoded());
      Key userKey = km.generateAESKey();
      activeBuyerKeys.put(username, userKey);
      challenge.setKey(userKey.getEncoded());
      //challenge.setTicket(userKey);
      return km.encryptRSAObject(pk, challenge);
	}
	public void doLogout(String username)
		throws RemoteException
	{
		if(activeBuyerKeys.containsKey(username))
			activeBuyerKeys.remove(username);
		else if (activeSellerKeys.containsKey(username))
			activeSellerKeys.remove(username);
		return;
	}
}
