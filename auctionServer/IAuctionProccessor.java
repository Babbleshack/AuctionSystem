
package auctionServer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;
import java.security.*;
import javax.crypto.*;

import storageObjects.AuctionItem;
import storageObjects.Bid;
import storageObjects.Product;

public interface IAuctionProccessor extends Remote {


	/**
	 * Creates an Auction Takes a sealed AutcionItem
	 * ADD USERNAME TO AUCTIONITEM OBJECT
	 * @param  username        used to identify user
	 * @param  sealedAuction   contains AuctionItem encrypted and Signed
	 * @return SealedObject    Sealed AutionItem.
	 * @throws RemoteException 
	 */
	public SealedObject createAuction(String username, SealedObject sealedAuction)
		throws RemoteException;
	/**
	 * Closes an auction returning sealed AuctionObject
	 * @param  username        used to identify user
	 * @param  sealedAuction   Contains AuctionItem to add
	 * @return                 Sealed AuctionItem
	 * @throws RemoteException 
	 */
	public SealedObject closeAuction(String username, SealedObject sealedAuction) 
		throws RemoteException;
	/**
	 * Gets a list of auctions
	 * @param  username        Used to identify user
	 * @return                 Sealed list of auctions
	 * @throws RemoteException 
	 */
	public SealedObject getAuctions(String username)
		throws RemoteException;
	 /**
	  * Signs Seller client to sign, takes a username and sealed challenge.
	  * server decrypts challenge and generates AES Symetric key
	  * The key an challenge are Encrypted using RSA encryption and returned
	  * to client
	  * @param  username        Identifies user
	  * @param  sealedChallenge SealedObject Containing Challenge
	  * @return                 Sealed ChallengeRespons and AES Symetric key 'ticket'
	  * @throws RemoteException 
	  */
	public SealedObject sellerSignIn(String username, SealedObject sealedChallenge)
		throws RemoteException;
   /**
    * Signs Seller client to sign, takes a username and sealed challenge.
    * server decrypts challenge and generates AES Symetric key
    * The key an challenge are Encrypted using RSA encryption and returned
    * to client
    *  @param  username        Identifies user
    *  @param  sealedChallenge SealedObject Containing Challenge
    *  @return                 Sealed ChallengeRespons and AES Symetric key 'ticket'
    *  @throws RemoteException 
    */
	public SealedObject buyerSignIn(String username, SealedObject sealedChallenge)
		throws RemoteException;
	/**
	 * Makes a bid on an auction takes a sealed bid object
	 * @param  username        Identifies user
	 * @param  sealedBid       SealedObject containing Bid
	 * @throws RemoteException 
	 */
	public void makeBid(String username, SealedObject sealedBid) 
		throws RemoteException;
	/**
	 * removes user from list of active users
	 * @param  username        Identifies User
	 * @throws RemoteException
	 */
	public void doLogout(String username)
		throws RemoteException;
}