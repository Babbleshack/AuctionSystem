package auctionServer;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import auctionServer.AuctionProccessor;
public class startServer {
  public startServer()
  {
    try {
            AuctionProccessor server = new AuctionProccessor();
            Naming.rebind("rmi://localhost/AuctionService", server);
            System.out.println("SERVER STARTED");
    } catch (RemoteException e) {
            System.err.println("Error creating AuctionProccessor" + "\n");
            e.printStackTrace();
    } catch (MalformedURLException e) {
            System.err.println("Error Bad URL!" + "\n");
            e.printStackTrace();
    }
  }
  public static void main(String args[]) {
          new startServer();	
  }
}
