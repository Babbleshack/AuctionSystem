package storageObjects;

import javax.crypto.*;
import java.security.*;
import java.io.Serializable;
public class Challenge implements Serializable {
	private String username;
	private int challenge;
	private byte[] key;
	public Challenge(String username)
	{
		this.username = username;
	}
	public String getUsername()
	{
		return this.username;
	}
	public void setChallenge(int challenge)
	{
		this.challenge = challenge;
	}
	public int getChallenge()
	{
		return challenge;
	}
	public void setKey(byte[] key)
	{
		this.key = key;
	}
	public byte[] getKey()
	{
		return key;
	}
}