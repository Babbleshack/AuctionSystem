/**
 * Encapsulates bids 
 * a bid is 
 */
package storageObjects;
import java.io.Serializable;
import java.rmi.server.UID;
public class Bid implements Serializable
{
	private String productUID;
	double bidValue;
	private Profile bidder;
	public static final double FALSE_VALUE = -1;
	public Bid(String uid, Profile profile, double bidValue)
	{
		this.productUID = uid;
		this.bidder = profile;
		this.bidValue = bidValue;
	}
	public Profile getBidder() {
	    return this.bidder;
	}
	public String getBidderName() {
		return this.bidder.getName();
	}
	public String getBidderEmail(){
		return this.bidder.getEmail();
	}
	public double getBidValue() {
		return this.bidValue;
	}
	public String getProductUID()
	{
		return this.productUID;
	}
}