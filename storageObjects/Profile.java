/**
 * encapsulates users details, serializable
 */
package storageObjects;

import java.io.Serializable;

public class Profile implements Serializable
{
	private String name;
	private String email;
	public Profile(String name, String email)
	{
		this.name = name;
		this.email = email;
	}
	public String getName() {
	    return this.name;
	}
	public String getEmail() {
	    return this.email;
	}
}