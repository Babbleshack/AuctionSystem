/**
 * AuctionItem encapsulate auction Item Data
 */
package storageObjects;
import java.io.Serializable;
import java.rmi.server.UID;
import java.util.Random;
public class AuctionItem implements Serializable
{
	private String auctionUID;
	private String productTitle;
	private Bid highestBid;
	private double reservePrice;
	private String owner;
	private final static int ID_LENGTH = 4;
	private final static char[] IDCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();



	public AuctionItem(String productTitle, double reservePrice, String owner)
	{
		this.auctionUID = randomID(ID_LENGTH);
		this.productTitle = productTitle;
		this.reservePrice = reservePrice;
		this.highestBid = new Bid("NO_ID", new Profile("NO_NAME", "NO_EMAIL"), Bid.FALSE_VALUE);
		this.owner = owner;
	}
	public AuctionItem(String productTitle, double reservePrice)
	{
		this.auctionUID = randomID(ID_LENGTH);
		this.productTitle = productTitle;
		this.reservePrice = reservePrice;
		this.highestBid = new Bid("NO_ID", new Profile("NO_NAME", "NO_EMAIL"), Bid.FALSE_VALUE);
	}	
	public AuctionItem(String UID)
	{
		this.auctionUID = UID;
	}
	/**
	 * returns true if bid is higher than current highest bid value
	 * @param  bid Bid
	 * @return     boolean
	 */
	public Boolean isBidHigher(Bid bid)
	{
		if(this.reservePrice > bid.getBidValue())
			return false;
		return bid.getBidValue() > this.highestBid.getBidValue();
	}
	/**
	 * return true if successfull
	 * false otherwise
	 * @param  bid [description]
	 * @return     [description]
	 */
	public Boolean setBid(Bid bid)
	{
		if(!isBidHigher(bid))
			return false;
		this.highestBid = bid;
		return true;
	}
	public Bid getBid()
	{
		return this.highestBid;
	}
	public double getBidValue()
	{
		return this.highestBid.getBidValue();
	}
	public String getUID() {
		// TODO Auto-generated method stub
		return this.auctionUID;
	}
	public String getProductTitle() {
		return productTitle;
	}
	public double getReservePrice() {
		return reservePrice;
	}
	private static String randomID(int len) {
	    char[] id = new char[len];
	    Random r = new Random(System.currentTimeMillis());
	    for (int i = 0;  i < len;  i++) {
	        id[i] = IDCHARS[r.nextInt(IDCHARS.length)];

	    }
	    return new String(id);
	}
	public String getOwner()
	{
		return this.owner;
	}
	public void setOwner(String owner)
	{
		this.owner = owner;
	}

}