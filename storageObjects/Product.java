/**
 * encapsulate product data
 */
package storageObjects;
import java.io.Serializable;
import java.rmi.server.UID;
public class Product implements Serializable
{
	private String title;
	String auctionUID;
	double currentBidValue;
	public Product(String title, String auctionUID, double currentBidValue)
	{
		this.title = title;
		this.auctionUID = auctionUID;
		this.currentBidValue = currentBidValue;
	}
	public String getTitle() {
		return title;
	}
	public String getAuctionUID() {
		return auctionUID;
	}
	public double getCurrentBidValue() {
		return currentBidValue;
	}
}